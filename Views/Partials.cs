
using System;
namespace examen.Views
{
    public class Partials
    {
        public static void HandleException(Exception exception, String Message = "")
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"{exception.Message} - {Message}");
            Console.ResetColor();
        }
    }

   
}