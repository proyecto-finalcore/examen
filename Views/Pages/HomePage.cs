using System;
namespace examen.Views.Pages
{
    public class HomePage
    {
        public AdminPage adminPage { get; set; }
        public HomePage(Services.UserServices userServices)
        {
            this.adminPage = new AdminPage(userServices);
        }
        public void OnInit()
        {
            PrintMenu();
            int option = this.GetOption();
            this.BuildMenu(option);
        }

        public int GetOption()
        {
            Console.Write("\n\t\tSelecciona una opcion: ");
            return int.Parse(Console.ReadLine());
        }

        private void PrintMenu()
        {
            Console.WriteLine("\t\t1 - Admin\n\t\t2 - Software");
        }
        public void BuildMenu(int option)
        {
            Console.Clear();
            switch (option)
            {
                case 1:
                    this.adminPage.OnInit();
                break;
                default:
                    Console.WriteLine("\t\tLa opcion que selecciono es inexistente");
                    this.OnInit();
                break;
            }
        }
    }
}