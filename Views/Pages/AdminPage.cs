using System;
using examen.Services;
using examen.Views.Components;
namespace examen.Views.Pages
{
    public class AdminPage
    {
        UpdaterUserComponent updaterUserComponent;
        RegisterUserComponent registerUserComponent;

        string PasswordAdmin = "Admin123.";

        public AdminPage(UserServices userServices)
        {
            this.updaterUserComponent = new UpdaterUserComponent(userServices);
            this.registerUserComponent = new RegisterUserComponent(userServices);

            
        }

        public void OnInit()
        {
           System.Console.Write("\t\tFavor digite su contraseña: ");
           string PasswordA = Console.ReadLine();
           Console.Clear();
           if  (PasswordAdmin == PasswordA)
           {
                this.PrintMenu();
                int option =  this.GetOption();
                this.Router(option);
           }
           else 
           {
               System.Console.WriteLine("\t\tContraseña incorrecta");
               this.OnInit();

           }

        }

        private int GetOption()
        {
            Console.Write("\t\tSeleccione una opcion: ");
            return int.Parse(Console.ReadLine());
        }

        public void PrintMenu()
        {
            Console.WriteLine("\t\t1 - Crear Usuario\n\t\t2 - Actualizar Usuario");
        }

        private void Router(int route)
        {
            switch (route)
            {
                case 1:
                    this.registerUserComponent.OnInit();
                break;
                case 2:
                    this.updaterUserComponent.OnInit();
                break;
                default:
                    this.OnInit();
                break;
            }
        }
    }
}