using System;
using examen.Context;
using examen.Services;
using Examen.Models;

namespace examen.Views.Components
{
    public class UpdaterUserComponent
    {
        public UserServices UserServices { get; }

        public UpdaterUserComponent(UserServices userServices)
        {
            UserServices = userServices;
        }
        public void OnInit()
        {
            var user = this.GetUserToModifie();
            this.UpdaterDataUser(user);
        }

        private User GetUserToModifie()
        {
            Console.Write("Id de usuario: ");
            var user = this.UserServices.GetUserById(int.Parse(Console.ReadLine()));

            return user;
        }

        private void UpdaterDataUser(User user)
        {
            Console.Write("Nickname: ");
            user.Nickname = Console.ReadLine();

            Console.Write("Email: ");
            user.Email = Console.ReadLine();

            Console.Write("Password: ");
            user.Password = Console.ReadLine();

            this.UserServices.UpdateUser(user);
        }
    }
}