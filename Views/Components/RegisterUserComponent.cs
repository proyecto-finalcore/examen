using System;
using examen.Services;
using Examen.Models;
namespace examen.Views.Components
{
    public class RegisterUserComponent
    {
        public RegisterUserComponent(UserServices userServices)
        {
            UserServices = userServices;
        }

        public UserServices UserServices { get; }

        public void OnInit()
        {
            this.GetDataUser();
        }



        private void GetDataUser()
        {
            User user = new User();
            
            try
            {
                Console.Write("\n\t\tNickname: ");
                    user.Nickname = Console.ReadLine();

                    Console.Write("\t\tEmail: ");
                    user.Email = Console.ReadLine();

                    Console.Write("\t\tPassword: ");
                    user.Password = Console.ReadLine();

                    

                    if(user.Nickname == "" || user.Email == "" ||)
                    {
                        Console.Clear();
                        this.GetDataUser();
                    }

                this.UserServices.AddUser(user);
            }
            catch (Exception e)
            {
                Partials.HandleException(e, "Error al ingresar usuario");
            }

        }
    }
}