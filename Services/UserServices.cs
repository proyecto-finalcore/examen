using System.Collections.Generic;
using System.Linq;
using examen.Context;
using examen.States;
using Examen.Models;
using Microsoft.EntityFrameworkCore;

namespace examen.Services
{
    public class UserServices
    {
        public ApplicationDbContext db { get; set; }

        public UserServices(ApplicationDbContext context)
        {
            db = context;
        }

        public List<User> ListUser()
        {
            return db.Users.ToList();
        } 

        public User GetUserById(int UserId)
        {
            var User = db.Users.FirstOrDefault(x => x.Id == UserId);
            return User;
        }
        public User GetUserByData(string user, string password)
        {
            var User = db.Users.FirstOrDefault(x => x.Email == user && x.Password == password);
            ApplicationState.IdUserLogged = User.Id;
            return User;
        }

        public void AddUser(User user)
        {
            db.Users.Add(user);
            db.SaveChanges();
        }

        public User GetUserInformationContact (int Id)
        {
            return db.Users.Include(x => x.Contacts).FirstOrDefault(User => User.Id == Id);
        }

        public void UpdateUser(User user)
        {
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();
        }

    }
}