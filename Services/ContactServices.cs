using System.Collections.Generic;
using System.Linq;
using examen.Context;
using Examen.Models;

namespace examen.Services
{
    public class ContactServices
    {
        public ApplicationDbContext db { get; set; }

        public ContactServices(ApplicationDbContext context)
        {
            db = context;
        }
        public void AddContact( Contacts contact)
        {
            db.Contacts.Add(contact);
            db.SaveChanges();
        }

    }
}