using System.Collections.Generic;
using System.Data.Common;
using examen.Context;
using Examen.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
namespace examen.Services
{
    public class MessageServices
    {
        public ApplicationDbContext db { get; set;}
        public MessageServices(ApplicationDbContext context)
        {
            db = context;
        }

        public List<Message> ListMessage()
        {
            return db.Messages.ToList();
        }

        public Message GetMessageById(int id)
        {
            return db.Messages.FirstOrDefault(x => x.Id == id);
        }
        public void AddMessage(Message message)
        {
            db.Messages.Add(message);
            db.SaveChanges();
        }

        public List<Message> GetUserRecivedMessage(int UserId)
        {
            return db.Messages.Where(x => x.ContactId == UserId).ToList();
        }

        public List<Message> GetUserSendedMessage(int UserId)
        {
            return db.Messages.Where(x => x.ContactId != UserId).ToList();
        }

        public bool DeleteMessageById(int id)
        {
            var message = db.Messages.FirstOrDefault(x => x.Id == id);

            if(message == null)
            {
                return false;
            }
            
            db.Messages.Remove(message);
            db.SaveChanges();
            return true;

        }

        public bool UpdateMessageById(int id, Message message)
        {
            var messageOpteined = db.Messages.FirstOrDefault(x => x.Id == id);

            if(messageOpteined == null)
            {
                return false;
            }

            db.Entry(message).State = EntityState.Modified;
            db.SaveChanges();
            return true;

        }

    }
}