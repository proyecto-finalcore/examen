using Microsoft.EntityFrameworkCore;
using Examen.Models;
namespace examen.Context
{
    public class ApplicationDbContext :  DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Contacts> Contacts { get; set; }
        public DbSet<Message> Messages { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=localhost; Database=ExamenDB; User Id=sa; Password=gyfcyd-nIggyd-9pemde");
        }
    }
}