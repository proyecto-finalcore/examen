using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Examen.Models
{
    public class Contacts
    {
        [Key]
        public int Id { get; set; }

        [StringLength(30)]
        [ForeignKey("User")]
        public int UserId { get; set; }
        
        public string Email { get; set; }

        public List<Message> Messages { get; set; }
    }
}