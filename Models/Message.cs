using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Examen.Models
{
    public class Message
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
        [ForeignKey("Contacts")]
        public int ContactId { get; set; }
        [Required]
        [StringLength(20)]
        public string Affair { get; set; }
        [Required]
        [MaxLength]
        public string MessageUser { get; set; }
        [Required]
        public bool State { get; set; }
        
    }
}